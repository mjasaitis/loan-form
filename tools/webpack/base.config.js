const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");

module.exports = (env, argv) => {
    return {
        entry: ["./src/js/index.js", "./src/sass/main.scss"],
        module: {
            rules: [
                {
                    exclude: /node_modules/,
                    test: /.(js|jsx)$/,
                    use: ["babel-loader"]
                },
                {
                    test: /\.scss$/,
                    use: [
                        // fallback to style-loader in development
                        argv.mode !== "production" ? "style-loader" : MiniCssExtractPlugin.loader,
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                data: ' $publicPath : "' + env.PUBLIC_DIR + '"; ',
                                sourceMap: true
                            }
                        }
                    ]
                }
            ]
        },
        resolve: {
            extensions: [".js", ".jsx"]
        }
    };
};
