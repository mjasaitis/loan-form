const webpack = require("webpack");
const merge = require("webpack-merge");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const baseConfig = require("./base.config.js");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env,argv) => {
	return merge(baseConfig(env, argv), {
		output: {
			path: path.join(__dirname, "../../public/"),
			filename: "js/[name].[chunkhash].bundle.js",
			chunkFilename: "js/[name].[chunkhash].bundle.js"
		},

		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						test: /node_modules/,
						chunks: "initial",
						name: "vendor",
						enforce: true
					}
				}
			}
		},

		plugins: [
			new UglifyJsPlugin({
				sourceMap: false,
				uglifyOptions: {
					output: {
						comments: false
					}
				}
			}),
			new webpack.DefinePlugin({
				"process.env": {
					PUBLIC_DIR: JSON.stringify(env.PUBLIC_DIR)
				}
			}),

			new MiniCssExtractPlugin({
				// Options similar to the same options in webpackOptions.output
				// both options are optional
				path: path.join(__dirname, "../../public/"),
				filename: "style/[name].css",
				// chunkFilename: "style/[id].css"
			}),

			new HtmlWebpackPlugin({
				template: path.join(__dirname, "../../src/html/index.html"),
				inject: true,
				publicDir: env.PUBLIC_DIR
			})

		]
	});
};
