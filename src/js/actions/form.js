import history from "../components/history";

export const SAVE_FORM_STEP1 = "SAVE_FORM_STEP1";
export const SAVE_FORM_STEP2 = "SAVE_FORM_STEP2";
export const SAVE_FORM_STEP3 = "SAVE_FORM_STEP3";
export const SAVE_FORM_STEP4 = "SAVE_FORM_STEP4";

export function gotoStep(step, props) {
	return (dispatch, getState) => {
		const config = getState().config;
		let type_;

		switch (step - 1) {
			case 1:
				type_ = SAVE_FORM_STEP1;
				break;
			case 2:
				type_ = SAVE_FORM_STEP2;
				break;
			case 3:
				type_ = SAVE_FORM_STEP3;
				break;
			case 4:
				type_ = SAVE_FORM_STEP4;
				break;
		}

		dispatch({ type: type_, payload: props });

		const path = step != 5 ? `step${step}` : "summary";
		history.push(`${config.publicDir}${path}`);
	};
}
