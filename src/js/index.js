import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import { Router } from "react-router-dom";
import reducers from "./reducers";
import promise from "redux-promise";
import thunk from "redux-thunk";
import Routes from "./routes";

import history from "./components/history";

import './../sass/main.scss';

const createStoreWithMiddleware = applyMiddleware(promise, thunk)(createStore);

ReactDOM.render(
	<Provider store={createStoreWithMiddleware(reducers)}>
		<Router history={history}>
			<Routes />
		</Router>
	</Provider>,
	document.querySelector("#container")
);
