import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import App from "./components/app";

import FormIntro from "./components/form/form_intro";
import FormStep1 from "./components/form/form_step1";
import FormStep2 from "./components/form/form_step2";
import FormStep3 from "./components/form/form_step3";
import FormStep4 from "./components/form/form_step4";
import FormSummary from "./components/form/form_summary";

const publicDir = process.env.PUBLIC_DIR;

export default () => (
	<App>
		<Switch>
			<Route path={`${publicDir}/`} component={FormIntro} />
			<Route path={`${publicDir}/index.html`} component={FormIntro} />
			<Route exact path={`${publicDir}step1`} component={FormStep1} />
			<Route exact path={`${publicDir}step2`} component={FormStep2} />
			<Route exact path={`${publicDir}step3`} component={FormStep3} />
			<Route exact path={`${publicDir}step4`} component={FormStep4} />
			<Route exact path={`${publicDir}summary`} component={FormSummary} />
		</Switch>
	</App>
);
