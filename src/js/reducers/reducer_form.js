import {
	SAVE_FORM_STEP1,
	SAVE_FORM_STEP2,
	SAVE_FORM_STEP3,
	SAVE_FORM_STEP4
} from "./../actions/form";

const INITIAL_STATE = {
	// currentStep: 1,
	completedStep: 0,
	stepsTitles: ["Paskola", "Pajamos", "Įsipareigojimai", "Asmens duomenys"],

	loanAmount: 0,
	loanAmountMin: 0,
	loanAmountMax: 20000,

	loanDuration: 6,
	loanDurationMin: 6,
	loanDurationMax: 60,

	loanWithCodebtor: false,

	income: 245,
	incomeMin: 245,
	incomeMax: 3000,

	codebtorIncome: 0,
	codebtorIncomeMin: 0,
	codebtorIncomeMax: 3000,

	haveLiabilities: false,

	consumerLoan: 0,
	consumerLoanMin: 0,
	consumerLoanMax: 30000,

	mortgageLoan: 0,
	mortgageLoanMin: 0,
	mortgageLoanMax: 87000,

	leasing: 0,
	leasingMin: 0,
	leasingMax: 14500,

	liabilitiesMonthlyAmount: 0,
	liabilitiesMonthlyAmountMin: 0,
	liabilitiesMonthlyAmountMax: 7000,

	personName: "",
	personLname: "",
	personCode: "",
	codebtorName: "",
	codebtorLname: "",
	codebtorCode: "",

	errors: {
		general: "Klaida. Patikrinkite formos duomenis.",

		loanAmount: "Pasirinkite norimą paskolos dydį.",
		loanDuration: "Pasirinkite norimą paskolos trukmę.",

		income: "Nurodykite grynasias mėnesio pajamas.",
		codebtorIncome: "Nurodykite bendraskolio grynasias mėnesio pajamas.",

		liabilities: "Nurodykite finansinius įsipareigojimus.",
		liabilitiesMonthlyAmount: "Nurodykite finansinių įsipareigojimų mėnesio įmokų sumą.",

		personName: "Įveskite vardą.",
		personLname: "Įveskite pavardę.",
		personCode: "Įveskite asmens kodą.",
		personCodeFormat: "Asmens kodą turi sudaryti 11 skaitmenų.",

		codebtorName: "Įveskite bendraskolio vardą.",
		codebtorLname: "Įveskite bendraskolio pavardę.",
		codebtorCode: "Įveskite bendraskolio asmens kodą.",
		codebtorCodeFormat: "Bendraskolio asmens kodą turi sudaryti 11 skaitmenų.",
	}
};

export default function(state = INITIAL_STATE, action) {
	let completedStep;

	switch (action.type) {
		case SAVE_FORM_STEP1:
			completedStep = state.completedStep > 1 ? state.completedStep : 1;
			return {
				...state,
				loanAmount: action.payload.loanAmount,
				loanDuration: action.payload.loanDuration,
				completedStep: completedStep
			};
		case SAVE_FORM_STEP2:
			completedStep = state.completedStep > 2 ? state.completedStep : 2;
			return {
				...state,
				income: action.payload.income,
				codebtorIncome: action.payload.codebtorIncome,
				loanWithCodebtor: action.payload.loanWithCodebtor,
				completedStep: completedStep
			};
		case SAVE_FORM_STEP3:
			completedStep = state.completedStep > 3 ? state.completedStep : 3;
			return {
				...state,
				haveLiabilities: action.payload.haveLiabilities,
				consumerLoan: action.payload.consumerLoan,
				mortgageLoan: action.payload.mortgageLoan,
				leasing: action.payload.leasing,
				liabilitiesMonthlyAmount: action.payload.liabilitiesMonthlyAmount,
				completedStep: completedStep
			};
		case SAVE_FORM_STEP4:
			completedStep = state.completedStep > 4 ? state.completedStep : 4;
			return {
				...state,
				personName: action.payload.personName,
				personLname: action.payload.personLname,
				personCode: action.payload.personCode,
				codebtorName: action.payload.codebtorName,
				codebtorLname: action.payload.codebtorLname,
				codebtorCode: action.payload.codebtorCode,
				completedStep: completedStep
			};
		default:
			return state;
	}
}
