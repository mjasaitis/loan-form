import { combineReducers } from "redux";

import reducerConfig from "./reducer_config";
import reducerForm from "./reducer_form";

const rootReducer = combineReducers({
	config: reducerConfig,
	form: reducerForm
});

export default rootReducer;
