import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import promise from "redux-promise";
import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import { shallow, mount } from "enzyme";

import reducers from "./../reducers";
import FormStepsConnected, { FormSteps } from "./../components/form/form_steps.js";

describe("FormSteps", () => {
	it("renders correctly", () => {
		const form = {
			stepsTitles: ["Paskola", "Pajamos", "Įsipareigojimai", "Asmens duomenys"]
		};

		const wrapper = shallow(<FormSteps form={form} />);
		expect(wrapper).toMatchSnapshot();
	});

	it("has 4 steps", () => {
		const createStoreWithMiddleware = applyMiddleware(promise, thunk)(createStore);
		const store = createStoreWithMiddleware(reducers);

		const wrapper = mount(
			<Provider store={store}>
				<FormStepsConnected />
			</Provider>
		);

		expect(wrapper.find("FormSteps").find("li").length).toBe(4);
	});
});
