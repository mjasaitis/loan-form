import React, { Component } from "react";
import { Provider } from "react-redux";
import { shallow, mount } from "enzyme";

import FormStep1Connected, { FormStep1 } from "./../components/form/form_step1.js";

describe("FormStep1", () => {
	let wrapper;
	let props;
	let instance;

	beforeEach(() => {
		props = {
			form: {
				loanAmount: 0,
				loanAmountMin: 0,
				loanAmountMax: 3000,

				loanDuration: 6,
				loanDurationMin: 6,
				loanDurationMax: 60
			}
		};

		wrapper = shallow(<FormStep1 {...props} />);
		instance = wrapper.instance();
	});

	it("valid form", () => {
		wrapper.setState({
			loanAmount: 500
		});

		const errors = instance.getFormErrors();
		expect(instance.isFormValid(errors)).toBe(true);
	});

	it("invalid form", () => {
		const errors = instance.getFormErrors();
		expect(instance.isFormValid(errors)).toBe(false);
	});
});
