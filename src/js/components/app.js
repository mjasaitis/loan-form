import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import history from "./history";
import {} from "./../lib/helpers";

class App extends Component {
	componentWillMount() {
		let path = this.props.location.pathname;
		path = path.substr( this.props.config.publicDir.length );

		if (/^step\d+$/.test(path)) {
			const step = path.match(/\d+/)[0];
			if (this.props.form.completedStep < step) {
				history.replace(`${this.props.config.publicDir}step1`);
			}
		}

		if (/^summary$/.test(path)) {
			if (this.props.form.completedStep < this.props.form.stepsTitles.length) {
				history.replace(`${this.props.config.publicDir}step1`);
			}
		}
	}

	render() {
		return (
			<div>
				<div className="container-fluid">{this.props.children}</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default withRouter(connect(mapStateToProps)(App));
