import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import FormStep from "./form_step";

class FormSummary extends FormStep {
	render() {
		return (
			<div>
				<h1 className="primary">Anketos duomenys</h1>
				<div className="summary clearfix">
					<div className="cell cell-header">
						<NavLink to={`${this.props.config.publicDir}step1`} className="float-right">
							keisti
						</NavLink>
						<h4>Paskola</h4>
					</div>

					<div className="cell">Pageidaujama paskolos suma:</div>
					<div className="cell cell-result">€ {this.props.form.loanAmount.formatNumber()}</div>

					<div className="cell">Paskolos terminas:</div>
					<div className="cell  cell-result">
						{this.formatMonthsTitle(this.props.form.loanDuration)}
					</div>

					<div className="cell cell-header">
						<NavLink to={`${this.props.config.publicDir}step2`} className="float-right">
							keisti
						</NavLink>
						<h4>Informacija apie jūsų pajamas</h4>
					</div>

					<div className="cell">Paskolą imsiu:</div>
					<div className="cell cell-result">
						{this.props.form.loanWithCodebtor
							? "Su sutuoktiniu (bendraskoliu)"
							: "Vienas"}
					</div>
					<div className="cell">Jūsų grynosios mėnesio pajamos:</div>
					<div className="cell cell-result">€ {this.props.form.income.formatNumber()}</div>
					{this.props.form.loanWithCodebtor ? (
						<div className="cell">
							Sutuoktinio (bendraskolio) grynosios mėnesio pajamos:
						</div>
					) : null}
					{this.props.form.loanWithCodebtor ? (
						<div className="cell cell-result">€ {this.props.form.codebtorIncome.formatNumber()}</div>
					) : null}

					<div className="cell cell-header">
						<NavLink to={`${this.props.config.publicDir}step3`} className="float-right">
							keisti
						</NavLink>
						<h4>
							{this.props.form.loanWithCodebtor
								? "Informacija apie jūsų ir bendraskolio finansinius įsipareigojimus"
								: "Informacija apie jūsų (šeimos) finansinius įsipareigojimus"}
						</h4>
					</div>
					{this.props.form.haveLiabilities ? null : (
						<div className="cell cell-full">Finansinių įsipareigojimų neturite</div>
					)}
					{this.props.form.haveLiabilities ? (
						<div className="cell">Likusi grąžinti vartojimo kreditų suma:</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell cell-result">€ {this.props.form.consumerLoan.formatNumber()}</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell">Likusi grąžinti būsto finansavimo suma:</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell cell-result">€ {this.props.form.mortgageLoan.formatNumber()}</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell">
							Likusi grąžinti finansavimo pagal lizingo sutartį (-is) suma:
						</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell cell-result">€ {this.props.form.leasing.formatNumber()}</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell">
							Paminėtų finansinių įsipareigojimų mėnesio įmokų suma:
						</div>
					) : null}
					{this.props.form.haveLiabilities ? (
						<div className="cell cell-result">
							€ {this.props.form.liabilitiesMonthlyAmount.formatNumber()}
						</div>
					) : null}

					<div className="cell cell-header">
						<NavLink to={`${this.props.config.publicDir}step4`} className="float-right">
							keisti
						</NavLink>
						<h4>Asmens duomenys</h4>
					</div>
					<div className="cell">Vardas:</div>
					<div className="cell cell-result">{this.props.form.personName}</div>
					<div className="cell">Pavardė:</div>
					<div className="cell cell-result">{this.props.form.personLname}</div>
					<div className="cell">Asmens kodas:</div>
					<div className="cell cell-result">{this.props.form.personCode}</div>

					{this.props.form.loanWithCodebtor ? (
						<div className="cell cell-header">
							<NavLink
								to={`${this.props.config.publicDir}step4`}
								className="float-right"
							>
								keisti
							</NavLink>
							<h4>Bendraskolio asmens duomenys</h4>
						</div>
					) : null}

					{this.props.form.loanWithCodebtor ? <div className="cell">Vardas:</div> : null}
					{this.props.form.loanWithCodebtor ? (
						<div className="cell cell-result">{this.props.form.codebtorName}</div>
					) : null}
					{this.props.form.loanWithCodebtor ? <div className="cell">Pavardė:</div> : null}
					{this.props.form.loanWithCodebtor ? (
						<div className="cell cell-result">{this.props.form.codebtorLname}</div>
					) : null}
					{this.props.form.loanWithCodebtor ? (
						<div className="cell">Asmens kodas:</div>
					) : null}
					{this.props.form.loanWithCodebtor ? (
						<div className="cell cell-result">{this.props.form.codebtorCode}</div>
					) : null}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, {})(FormSummary);
