import React, { Component } from "react";

class FormError extends Component {
	render() {
		if (!this.props.msg) {
			return null;
		}
		return <div className="errorMsg">{this.props.msg}</div>;
	}
}

export default FormError;
