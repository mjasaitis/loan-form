import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import InputRange from "react-input-range";

import FormStep from "./form_step";
import FormSteps from "./form_steps";
import FormError from "./form_error";
import { gotoStep } from "./../../actions/form";

class FormStep2 extends FormStep {
	constructor(props, context) {
		super(props);
		this.state = {
			income: 0,
			codebtorIncome: 0,
			loanWithCodebtor: false,
			errorMsg: "",
			errors: {}
		};

		this.onIncomeChange = this.onIncomeChange.bind(this);
		this.onCodebtorIncomeChange = this.onCodebtorIncomeChange.bind(this);
	}

	onIncomeChange(value) {
		this.setState({
			income: value
		});
	}
	onCodebtorIncomeChange(value) {
		this.setState({
			codebtorIncome: value
		});
	}

	loanWithCodebtorChange(value) {
		this.setState({
			loanWithCodebtor: value
		});
	}

	componentWillMount() {
		this.setState({
			income: this.props.form.income,
			codebtorIncome: this.props.form.codebtorIncome,
			loanWithCodebtor: this.props.form.loanWithCodebtor
		});
	}

	getFormErrors() {
		let errors = {};

		if (!this.state.income) {
			errors.income = true;
		}

		if (this.state.loanWithCodebtor && !this.state.codebtorIncome) {
			errors.codebtorIncome = true;
		}

		return errors;
	}

	render() {
		return (
			<div>
				<h1 className="primary">Vartojimo paskola</h1>
				<FormSteps currentStep={2} />
				<div className="form-content">
					<h4 className="mb-4">Informacija apie jūsų pajamas</h4>
					<hr />
					<div className="pb-4">
						<FormError msg={this.state.errorMsg} />
					</div>

					<div className="row">
						<div className="col-4 col-sm-5 text-right">Paskolą imsiu</div>
						<div className="col-8 col-sm-7">
							<div className="float-left">
								<label className="mr-3 radio-label">
									<input
										className="radio"
										type="radio"
										name="loanWithCodebtor"
										checked={!this.state.loanWithCodebtor}
										onChange={e => {
											this.loanWithCodebtorChange(false);
										}}
									/>
									<span>Vienas</span>
								</label>
							</div>
							<div className="float-left">
								<label className="radio-label">
									<input
										type="radio"
										name="loanWithCodebtor"
										checked={this.state.loanWithCodebtor}
										onChange={e => {
											this.loanWithCodebtorChange(true);
										}}
									/>
									<span>Su sutuoktiniu (bendraskoliu)</span>
								</label>
							</div>
						</div>
					</div>
					<hr className="pb-4" />
					<div className="row slider-row mt-4">
						<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
							<label className={this.state.errors.income ? "invalid-feedback" : ""}>
								Jūsų grynosios mėnesio pajamos<span className="req">*</span>
							</label>
						</div>
						<div className="col-10 offset-1 col-sm-6 offset-sm-0">
							<InputRange
								value={this.state.income}
								minValue={this.props.form.incomeMin}
								maxValue={this.props.form.incomeMax}
								formatLabel={value => `${value.formatNumber()} €`}
								onChange={this.onIncomeChange}
							/>
						</div>
					</div>
					{this.state.loanWithCodebtor ? (
						<div className="row slider-row mt-4">
							<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
								<label
									className={
										this.state.errors.codebtorIncome ? "invalid-feedback" : ""
									}
								>
									Sutuoktinio (bendraskolio) grynosios mėnesio pajamos<span className="req">
										*
									</span>
								</label>
							</div>
							<div className="col-10 offset-1 col-sm-6 offset-sm-0">
								<InputRange
									value={this.state.codebtorIncome}
									minValue={this.props.form.codebtorIncomeMin}
									maxValue={this.props.form.codebtorIncomeMax}
									formatLabel={value => `${value.formatNumber()} €`}
									onChange={this.onCodebtorIncomeChange}
								/>
							</div>
						</div>
					) : null}
				</div>
				<div className="form-button-bar mb-2">
					<NavLink
						to={`${this.props.config.publicDir}step1`}
						className="btn btn-primary "
					>
						« Atgal
					</NavLink>

					<button
						className="btn btn-primary float-right"
						onClick={e => {
							this.goToNextStep(3);
						}}
					>
						Pirmyn »
					</button>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, { gotoStep })(FormStep2);
