import React, { Component } from "react";
import { connect } from "react-redux";
import history from "./../history";

export class FormSteps extends Component {
	constructor(props, context) {
		super(props);
	}

	linkClick(i) {
		if (this.props.form.completedStep >= i) {
			history.push(`${this.props.config.publicDir}step${i + 1}`);
		}
	}

	renderSteps() {
		if (!this.props.form) {
			return null;
		}

		return this.props.form.stepsTitles.map((title, i) => {
			const isCurrentStep = this.props.currentStep == i + 1;
			const passed = this.props.form.completedStep >= i + 1;
			const allowed = this.props.form.completedStep == i;

			let className = "";
			if (isCurrentStep) {
				className = "active";
			} else if (passed) {
				className = "passed";
			} else if (allowed) {
				className = "allowed";
			}

			return (
				<li key={i} className={className} onClick={e => this.linkClick(i)}>
					<span>{title}</span>
				</li>
			);
		});
	}

	render() {
		return <ol className="form-steps">{this.renderSteps()}</ol>;
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, {})(FormSteps);
