import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import InputRange from "react-input-range";

import FormStep from "./form_step";
import FormSteps from "./form_steps";
import FormError from "./form_error";
import { gotoStep } from "./../../actions/form";

class FormStep3 extends FormStep {
	constructor(props, context) {
		super(props);
		this.state = {
			haveLiabilities: false,
			consumerLoan: 0,
			mortgageLoan: 0,
			leasing: 0,
			liabilitiesMonthlyAmount: 0,
			errors: {}
		};

		this.onConsumerLoanChange = this.onConsumerLoanChange.bind(this);
		this.onMortgageLoanChange = this.onMortgageLoanChange.bind(this);
		this.onLeasingChange = this.onLeasingChange.bind(this);
		this.onLiabilitiesMonthlyAmountChange = this.onLiabilitiesMonthlyAmountChange.bind(this);
	}

	componentWillMount() {
		this.setState({
			haveLiabilities: this.props.form.haveLiabilities,
			consumerLoan: this.props.form.consumerLoan,
			mortgageLoan: this.props.form.mortgageLoan,
			leasing: this.props.form.leasing,
			liabilitiesMonthlyAmount: this.props.form.liabilitiesMonthlyAmount
		});
	}

	onConsumerLoanChange(value) {
		this.setState({
			consumerLoan: value
		});
	}

	onMortgageLoanChange(value) {
		this.setState({
			mortgageLoan: value
		});
	}

	onLeasingChange(value) {
		this.setState({
			leasing: value
		});
	}

	onLiabilitiesMonthlyAmountChange(value) {
		this.setState({
			liabilitiesMonthlyAmount: value
		});
	}

	haveLiabilitiesChange(value) {
		this.setState({
			haveLiabilities: value
		});
	}

	getFormErrors() {
		let errors = {};

		if (!this.state.haveLiabilities) {
			return errors;
		}

		if (!this.state.consumerLoan && !this.state.mortgageLoan && !this.state.leasing) {
			errors.liabilities = true;
		}

		if (!this.state.liabilitiesMonthlyAmount) {
			errors.liabilitiesMonthlyAmount = true;
		}

		return errors;
	}

	render() {
		return (
			<div>
				<h1 className="primary">Vartojimo paskola</h1>
				<FormSteps currentStep={3} />
				<div className="form-content">
					<h4 className="mb-4">
						{!this.props.form.loanWithCodebtor
							? "Informacija apie jūsų (šeimos) finansinius įsipareigojimus"
							: "Informacija apie jūsų ir bendraskolio finansinius įsipareigojimus"}
					</h4>
					<hr />

					<div className="pb-4">
						<FormError msg={this.state.errorMsg} />
					</div>

					<div className="row">
						<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
							<label>
								Ar turite banke „Swedbank“ arba kitose kredito įstaigose paimtų ir
								dar negrąžintų paskolų arba esate tokių paskolų laiduotojas?
							</label>
						</div>
						<div className="col-10 offset-1 col-sm-6 offset-sm-0">
							<div className="float-left">
								<label className="mr-3 radio-label">
									<input
										className="radio control-label"
										type="radio"
										name="formhaveLiabilities"
										checked={!this.state.haveLiabilities}
										onChange={e => {
											this.haveLiabilitiesChange(false);
										}}
									/>
									<span>Ne</span>
								</label>
							</div>
							<div className="float-left">
								<label className="radio-label">
									<input
										type="radio"
										name="formhaveLiabilities"
										checked={this.state.haveLiabilities}
										onChange={e => {
											this.haveLiabilitiesChange(true);
										}}
									/>
									<span>Taip</span>
								</label>
							</div>
						</div>
					</div>

					{this.state.haveLiabilities ? (
						<div>
							<div className="row slider-row mt-4">
								<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
									<label>Likusi grąžinti vartojimo kreditų suma</label>
								</div>
								<div className="col-10 offset-1 col-sm-6 offset-sm-0">
									<InputRange
										value={this.state.consumerLoan}
										minValue={this.props.form.consumerLoanMin}
										maxValue={this.props.form.consumerLoanMax}
										formatLabel={value => `${value.formatNumber()} €`}
										onChange={this.onConsumerLoanChange}
									/>
								</div>
							</div>

							<div className="row slider-row mt-4">
								<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
									<label>Likusi grąžinti būsto finansavimo suma</label>
								</div>
								<div className="col-10 offset-1 col-sm-6 offset-sm-0">
									<InputRange
										value={this.state.mortgageLoan}
										minValue={this.props.form.mortgageLoanMin}
										maxValue={this.props.form.mortgageLoanMax}
										formatLabel={value => `${value.formatNumber()} €`}
										onChange={this.onMortgageLoanChange}
									/>
								</div>
							</div>
							<div className="row slider-row mt-4">
								<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
									<label>
										Likusi grąžinti finansavimo pagal lizingo sutartį (-is) suma
									</label>
								</div>
								<div className="col-10 offset-1 col-sm-6 offset-sm-0">
									<InputRange
										value={this.state.leasing}
										minValue={this.props.form.leasingMin}
										maxValue={this.props.form.leasingMax}
										formatLabel={value => `${value.formatNumber()} €`}
										onChange={this.onLeasingChange}
									/>
								</div>
							</div>
							<div className="row slider-row mt-4">
								<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
									<label
										className={
											this.state.errors.liabilitiesMonthlyAmount
												? "invalid-feedback"
												: ""
										}
									>
										Paminėtų finansinių įsipareigojimų mėnesio įmokų suma
										<span className="req">*</span>
									</label>
								</div>
								<div className="col-10 offset-1 col-sm-6 offset-sm-0">
									<InputRange
										value={this.state.liabilitiesMonthlyAmount}
										minValue={this.props.form.liabilitiesMonthlyAmountMin}
										maxValue={this.props.form.liabilitiesMonthlyAmountMax}
										formatLabel={value => `${value.formatNumber()} €`}
										onChange={this.onLiabilitiesMonthlyAmountChange}
									/>
								</div>
							</div>
						</div>
					) : null}
				</div>
				<div className="form-button-bar mb-2">
					<NavLink
						to={`${this.props.config.publicDir}step2`}
						className="btn btn-primary "
					>
						« Atgal
					</NavLink>

					<button
						className="btn btn-primary float-right"
						onClick={e => {
							this.goToNextStep(4);
						}}
					>
						Pirmyn »
					</button>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, { gotoStep })(FormStep3);
