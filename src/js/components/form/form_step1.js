import React, { Component } from "react";
import { connect } from "react-redux";

import history from "./../history";
import InputRange from "react-input-range";

import FormStep from "./form_step";
import { gotoStep } from "./../../actions/form";
import FormSteps from "./form_steps";
import FormError from "./form_error";

export class FormStep1 extends FormStep {
	constructor(props, context) {
		super(props);
		this.state = {
			loanAmount: 0,
			loanDuration: 0,
			errorMsg: null,
			errors: {}
		};

		this.onLoanAmountChange = this.onLoanAmountChange.bind(this);
		this.onLoanDurationChange = this.onLoanDurationChange.bind(this);
	}

	onLoanAmountChange(value) {
		this.setState({
			loanAmount: value
		});
	}

	onLoanDurationChange(value) {
		this.setState({
			loanDuration: value
		});
	}

	componentWillMount() {
		this.setState({
			loanAmount: this.props.form.loanAmount,
			loanDuration: this.props.form.loanDuration
		});
	}

	getFormErrors() {
		let errors = {};

		if (!this.state.loanAmount) {
			errors.loanAmount = true;
		}

		if (!this.state.loanDuration) {
			errors.loanDuration = true;
		}

		return errors;
	}

	render() {
		if (!this.props.form) {
			return;
		}

		return (
			<div>
				<h1 className="primary">Vartojimo paskola</h1>
				<FormSteps currentStep={1} />
				<div className="form-content">
					<h4 className="mb-4">Paskolos informacija</h4>
					<hr />

					<div className="pb-4">
						<FormError msg={this.state.errorMsg} />
					</div>

					<div className="row slider-row mt-4">
						<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
							<label
								className={this.state.errors.loanAmount ? " invalid-feedback" : ""}
							>
								Pageidaujama paskolos suma<span className="req">*</span>
							</label>
						</div>
						<div className="col-10 offset-1 col-sm-6 offset-sm-0">
							<InputRange
								value={this.state.loanAmount}
								minValue={this.props.form.loanAmountMin}
								maxValue={this.props.form.loanAmountMax}
								formatLabel={value => `${value.formatNumber()} €`}
								onChange={this.onLoanAmountChange}
							/>
						</div>
					</div>
					<div className="row slider-row mt-4">
						<div className="col-10 offset-1 col-sm-5 offset-sm-0 col-md-4 offset-md-1 title-field">
							<label
								className={
									this.state.errors.loanDuration ? " invalid-feedback" : ""
								}
							>
								Paskolos terminas<span className="req">*</span>
							</label>
						</div>
						<div className="col-10 offset-1 col-sm-6 offset-sm-0">
							<InputRange
								value={this.state.loanDuration}
								minValue={this.props.form.loanDurationMin}
								maxValue={this.props.form.loanDurationMax}
								formatLabel={value => this.formatMonthsTitle(value)}
								onChange={this.onLoanDurationChange}
							/>
						</div>
					</div>
				</div>
				<div className="form-button-bar mb-2">
					<button
						className="btn btn-primary"
						onClick={e => {
							history.push(`${this.props.config.publicDir}`);
						}}
					>
						« Atgal
					</button>
					<button
						className="btn btn-primary float-right"
						onClick={e => {
							this.goToNextStep(2);
						}}
					>
						Pirmyn »
					</button>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, { gotoStep })(FormStep1);
