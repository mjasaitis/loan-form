import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import InputRange from "react-input-range";

import FormStep from "./form_step";
import FormSteps from "./form_steps";
import FormError from "./form_error";
import { gotoStep } from "./../../actions/form";

class FormStep2 extends FormStep {
	constructor(props, context) {
		super(props);

		this.state = {
			errors: {}
		};

		this.onPersonNameChange = this.onPersonNameChange.bind(this);
		this.onPersonLnameChange = this.onPersonLnameChange.bind(this);
		this.onPersonCodeChange = this.onPersonCodeChange.bind(this);

		this.onCodebtorNameChange = this.onCodebtorNameChange.bind(this);
		this.onCodebtorLnameChange = this.onCodebtorLnameChange.bind(this);
		this.onCodebtorCodeChange = this.onCodebtorCodeChange.bind(this);

		this.onSubmit = this.onSubmit.bind(this);
	}

	componentWillMount() {
		this.setState({
			personName: this.props.form.personName,
			personLname: this.props.form.personLname,
			personCode: this.props.form.personCode,
			codebtorName: this.props.form.codebtorName,
			codebtorLname: this.props.form.codebtorLname,
			codebtorCode: this.props.form.codebtorCode
		});
	}

	onPersonNameChange(e) {
		this.setState({
			personName: e.target.value
		});
	}

	onPersonLnameChange(e) {
		this.setState({
			personLname: e.target.value
		});
	}

	onPersonCodeChange(e) {
		this.setState({
			personCode: e.target.value
		});
	}

	onCodebtorNameChange(e) {
		this.setState({
			codebtorName: e.target.value
		});
	}

	onCodebtorLnameChange(e) {
		this.setState({
			codebtorLname: e.target.value
		});
	}

	onCodebtorCodeChange(e) {
		this.setState({
			codebtorCode: e.target.value
		});
	}

	getFormErrors() {
		let errors = {};

		if (!this.state.personName) {
			errors.personName = true;
		}

		if (!this.state.personLname) {
			errors.personLname = true;
		}

		if (!this.state.personCode) {
			errors.personCode = true;
		} else {
			if (!/^\d{11}$/.test(this.state.personCode)) {
				errors.personCodeFormat = true;
			}
		}

		if (this.props.form.loanWithCodebtor) {
			if (!this.state.codebtorName) {
				errors.codebtorName = true;
			}

			if (!this.state.codebtorLname) {
				errors.codebtorLname = true;
			}

			if (!this.state.codebtorCode) {
				errors.codebtorCode = true;
			} else {
				if (!/^\d{11}$/.test(this.state.codebtorCode)) {
					errors.codebtorCodeFormat = true;
				}
			}
		}
		return errors;
	}

	onSubmit(event) {
		event.preventDefault();
		this.goToNextStep();
	}

	goToNextStep() {
		const errors = this.getFormErrors();
		if (this.isFormValid(errors)) {
			this.props.gotoStep(5, this.state);
		} else {
			this.showError(errors);
		}
	}

	render() {
		return (
			<form onSubmit={this.onSubmit}>
				<h1 className="primary">Vartojimo paskola</h1>
				<FormSteps currentStep={4} />
				<div className="form-content">
					<h4 className="mb-4">Asmens duomenys</h4>
					<hr />

					<div className="pb-4">
						<FormError msg={this.state.errorMsg} />
					</div>

					<div className="row mt-2">
						<div className="col-5 col-md-4 offset-md-1 text-right col-form-label">
							<label
								className={this.state.errors.personName ? "invalid-feedback" : ""}
							>
								Vardas<span className="req">*</span>
							</label>
						</div>
						<div className="col-7 col-md-4">
							<input
								type="text"
								className={
									"form-control" +
									(this.state.errors.personName ? " is-invalid" : "")
								}
								onChange={this.onPersonNameChange}
								value={this.state.personName}
							/>
						</div>
					</div>
					<div className="row mt-2">
						<div className="col-5 col-md-4 offset-md-1 text-right  col-form-label">
							<label
								className={this.state.errors.personLname ? "invalid-feedback" : ""}
							>
								Pavardė<span className="req">*</span>
							</label>
						</div>
						<div className="col-7 col-md-4">
							<input
								type="text"
								className={
									"form-control" +
									(this.state.errors.personLname ? " is-invalid" : "")
								}
								onChange={this.onPersonLnameChange}
								value={this.state.personLname}
							/>
						</div>
					</div>
					<div className="row mt-2 mb-4 ">
						<div className="col-5 col-md-4 offset-md-1 text-right col-form-label">
							<label
								className={this.state.errors.personCode ? "invalid-feedback" : ""}
							>
								Asmens kodas<span className="req">*</span>
							</label>
						</div>
						<div className="col-7 col-md-4">
							<input
								maxLength="11"
								type="text"
								className={
									"form-control" +
									(this.state.errors.personCode ||
									this.state.errors.personCodeFormat
										? " is-invalid"
										: "")
								}
								onChange={this.onPersonCodeChange}
								value={this.state.personCode}
							/>
						</div>
					</div>

					{this.props.form.loanWithCodebtor ? (
						<div>
							<h4 className="mb-4 mt-4">Bendraskolio asmens duomenys</h4>
							<hr className="pb-3" />

							<div className="row mt-2">
								<div className="col-5 col-md-4 offset-md-1 text-right col-form-label">
									<label
										className={
											this.state.errors.codebtorName ? "invalid-feedback" : ""
										}
									>
										Vardas<span className="req">*</span>
									</label>
								</div>
								<div className="col-7 col-md-4">
									<input
										type="text"
										className={
											"form-control" +
											(this.state.errors.codebtorName ? " is-invalid" : "")
										}
										onChange={this.onCodebtorNameChange}
										value={this.state.codebtorName}
									/>
								</div>
							</div>
							<div className="row mt-2">
								<div className="col-5 col-md-4 offset-md-1 text-right col-form-label">
									<label
										className={
											this.state.errors.codebtorLname
												? "invalid-feedback"
												: ""
										}
									>
										Pavardė<span className="req">*</span>
									</label>
								</div>
								<div className="col-7 col-md-4">
									<input
										type="text"
										className={
											"form-control" +
											(this.state.errors.codebtorLname ? " is-invalid" : "")
										}
										onChange={this.onCodebtorLnameChange}
										value={this.state.codebtorLname}
									/>
								</div>
							</div>
							<div className="row mt-2 ">
								<div className="col-5 col-md-4 offset-md-1 text-right col-form-label">
									<label
										className={
											this.state.errors.codebtorCode ||
											this.state.errors.codebtorCodeFormat
												? "invalid-feedback"
												: ""
										}
									>
										Asmens kodas<span className="req">*</span>
									</label>
								</div>
								<div className="col-7 col-md-4">
									<input
										maxLength="11"
										type="text"
										className={
											"form-control" +
											(this.state.errors.codebtorCode ? " is-invalid" : "")
										}
										onChange={this.onCodebtorCodeChange}
										value={this.state.codebtorCode}
									/>
								</div>
							</div>
						</div>
					) : null}
				</div>
				<div className="form-button-bar mb-2">
					<NavLink
						to={`${this.props.config.publicDir}step3`}
						className="btn btn-primary "
					>
						« Atgal
					</NavLink>

					<button
						className="btn btn-primary float-right"
						onClick={e => {
							this.goToNextStep(2);
						}}
					>
						Pirmyn »
					</button>
				</div>
			</form>
		);
	}
}

function mapStateToProps(state) {
	return { config: state.config, form: state.form };
}

export default connect(mapStateToProps, { gotoStep })(FormStep2);
