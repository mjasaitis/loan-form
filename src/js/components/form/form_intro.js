import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

class FormIntro extends Component {
	render() {
		return (
			<article>
				<h1 className="primary">Vartojimo paskola</h1>
				<div className="form-content">
					<h2>Paskolos, kurios padeda!</h2>
					<h3>
						Specialus pasiūlymas! Jei turite Jums asmeniškai paskaičiuotą paskolos sumos
						pasiūlymą, pasinaudojus juo, sutarties administravimo mokestis bus 0 Eur.
					</h3>

					<p>
						Pasiūlymas galioja 2018 07 09 – 08 12. Prisijunkite prie interneto banko
						arba išmaniosios programėlės ir pasitikrinkite, ar turite pasiūlymą
					</p>
					<p>
						Vartojimo paskola pravers, jei planuojate atnaujinti namus, remontuoti
						automobilį, įsigyti naujų baldų, buitinės technikos, ar kitą brangesnį
						daiktą.
					</p>

					<ul>
						<li>Paskola nuo 500 iki 20 000 Eur be užstato.</li>
						<li>Pinigus pervesime Jums į sąskaitą iš karto po sutarties sudarymo.</li>
						<li>
							Pasiskolintą sumą ar jos dalį galėsite grąžinti anksčiau laiko be
							papildomų mokesčių.
						</li>
					</ul>
					<p>
						Atkreipiame dėmesį, kad naudodamiesi finansavimo paslaugomis Jūs prisiimate
						finansinius įsipareigojimus. Netinkamas finansinių įsipareigojimų vykdymas
						gali daryti neigiamą įtaką Jūsų kredito istorijai, pabranginti skolinimąsi,
						taip pat gali būti pradėtas priverstinis išieškojimas.
					</p>
				</div>
				<div className="form-button-bar text-right mb-2">
					<NavLink
						to={`${this.props.config.publicDir}step1`}
						className="btn btn-primary "
					>
						Pildyti anketą »
					</NavLink>
				</div>
			</article>
		);
	}
}

function mapStateToProps(state) {
	return {
		config: state.config
	};
}

export default connect(mapStateToProps, {})(FormIntro);
