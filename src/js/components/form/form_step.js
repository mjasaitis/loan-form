import React, { Component } from "react";

class FormStep extends Component {
	constructor(props) {
		super(props);

		this.goToNextStep = this.goToNextStep.bind(this);
	}

	formatMonthsTitle(value) {
		return (
			`${value} ` + ((value > 10 && value < 20) || value % 10 == 0 ? "mėnesių" : ( value % 10 == 1 ? "mėnesis" : "mėnesiai" ) )
		);
	}

	isFormValid(errors) {
		return Object.keys(errors).length ? false : true;
	}

	showError(errors) {
		const errKey = Object.keys(errors)[0];

		if (this.props.form.errors[errKey]) {
			this.setState({ errorMsg: this.props.form.errors[errKey] });
		} else {
			this.setState({ errorMsg: this.props.form.errors.general });
		}

		this.setState({ errors: errors });
		window.scroll(0,0);
	}

	goToNextStep(step) {
		const errors = this.getFormErrors();
		if (this.isFormValid(errors)) {
			this.props.gotoStep(step, this.state);
		} else {
			this.showError(errors);
		}
	}
}

export default FormStep;
